package com.vconnekt.project;


import com.vconnekt.project.Models.Datum;

import org.junit.Test;
import static org.junit.Assert.*;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class FirstUnitTest {


    // to simulate addition of object in list

    @Test
    public void addToListMock(){
        List<Datum> list = Mockito.mock(ArrayList.class);

        Datum datum = new Datum();

        datum.setYear("2008");
        datum.setTitle("Slumdog Millionare");
        datum.setSreial_number(1);
        datum.setPoster("https://images-na.ssl-images-amazon.com/images/I/81Fr8W4urwL._SX300_.jpg");
        datum.setId(1080);

        list.add(datum);

        Mockito.verify(list).add(datum);

        assertEquals(0,list.size());

        // result is verified as no object is added hence size of list remains 0

    }


    // actual addition of object in list

    @Test
    public void addToListSPY(){
        List<Datum> list = Mockito.spy(ArrayList.class);

        Datum datum = new Datum();

        datum.setYear("2008");
        datum.setTitle("Slumdog Millionare");
        datum.setSreial_number(1);
        datum.setPoster("https://images-na.ssl-images-amazon.com/images/I/81Fr8W4urwL._SX300_.jpg");
        datum.setId(1080);

        list.add(datum);

        Mockito.verify(list).add(datum);

        assertEquals(1,list.size());

        // result is verified as one object (datum) is added to the list hence size of list is now 1.q

    }

}
