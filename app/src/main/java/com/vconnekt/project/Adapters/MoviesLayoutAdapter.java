package com.vconnekt.project.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.vconnekt.project.Models.Datum;
import com.vconnekt.project.R;

import java.util.ArrayList;
import java.util.List;

public class MoviesLayoutAdapter extends RecyclerView.Adapter<MoviesLayoutAdapter.MoviesLayoutViewHolder> implements Filterable {

    List<Datum> moviewData = new ArrayList<>();
    List<Datum> copyOFMovieList = new ArrayList<>();
    Context context;

    @Override
    public MoviesLayoutViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view  = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_movies_layout,viewGroup,false);
        context = viewGroup.getContext();
        return new MoviesLayoutViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviesLayoutViewHolder holder, int i) {
        Datum datum = copyOFMovieList.get(i);
        holder.mTextViewGenre.setText(datum.getGenre());
        holder.mTextViewTitle.setText(datum.getTitle());
        holder.mTextViewYear.setText(datum.getYear());
        Glide.with(context).load(datum.getPoster()).placeholder(R.drawable.ic_launcher_background).into(holder.mImageView);
    }

    @Override
    public int getItemCount() {
        return copyOFMovieList.size();
    }

    public void setMoviewData(List<Datum> list){
        this.moviewData = list;
        this.copyOFMovieList = list;
        notifyDataSetChanged();
    }

    @Override
    public Filter getFilter() {
        return filter;
    }

    private Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            Log.d("fljevbfev",constraint.toString());
            if (constraint == null || constraint.length() == 0){
                 copyOFMovieList = moviewData;
            }
            else {
                List<Datum> list = new ArrayList<>();
                String query = constraint.toString().toLowerCase().trim();

                for (Datum datum : moviewData){
                    if (datum.getTitle().toLowerCase().startsWith(query) || datum.getGenre().toLowerCase().startsWith(query)){
                        list.add(datum);
                    }
                }

                copyOFMovieList = list;

            }

            FilterResults results = new FilterResults();
            results.values = copyOFMovieList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            copyOFMovieList = (List<Datum>) results.values;
            notifyDataSetChanged();

        }
    };




    public class MoviesLayoutViewHolder extends RecyclerView.ViewHolder {

        ImageView mImageView;
        TextView mTextViewGenre,mTextViewTitle,mTextViewYear;

        public MoviesLayoutViewHolder(@NonNull View itemView) {
            super(itemView);

            mImageView = itemView.findViewById(R.id.imageView);
            mTextViewGenre = itemView.findViewById(R.id.movie_genre);
            mTextViewTitle = itemView.findViewById(R.id.movie_title);
            mTextViewYear = itemView.findViewById(R.id.movie_year);

        }
    }

}
