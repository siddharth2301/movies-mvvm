package com.vconnekt.project.Dao;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.vconnekt.project.Models.Data;
import com.vconnekt.project.Models.Datum;

import java.util.List;

@Dao
public interface MoviesDao {

    @Query("SELECT * FROM movie_table")
    LiveData<List<Datum>> getAllMovies();

    @Insert
    void insert(Datum datum);

    @Query("DELETE FROM movie_table")
    void deleteAllMovies();

}
