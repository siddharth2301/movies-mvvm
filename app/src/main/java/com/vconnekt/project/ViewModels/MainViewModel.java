package com.vconnekt.project.ViewModels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import com.vconnekt.project.Models.Datum;
import com.vconnekt.project.Repositories.MainRepository;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    MutableLiveData<List<Datum>> liveData;
    MainRepository mMainRepository;
    private LiveData<List<Datum>> allMovies;

    public MainViewModel(Application application) {
        super(application);
        mMainRepository = MainRepository.getInstance(application);
        liveData = mMainRepository.getAllMovies();
        allMovies = mMainRepository.getAllData();
    }

    public LiveData<List<Datum>> getAllMoviesLiveData(){
        return liveData;
    }

    public LiveData<List<Datum>> getAllMovies(){
        return allMovies;
    }
}
