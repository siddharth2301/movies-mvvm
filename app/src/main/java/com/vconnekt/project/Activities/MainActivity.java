package com.vconnekt.project.Activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vconnekt.project.Adapters.MoviesLayoutAdapter;
import com.vconnekt.project.Models.Data;
import com.vconnekt.project.Models.Datum;
import com.vconnekt.project.R;
import com.vconnekt.project.ViewModels.MainViewModel;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    MainViewModel mMainViewModel;
    RecyclerView mRecyclerView;
    Toolbar mToolbar;
    SearchView mSearchView;
    MoviesLayoutAdapter adapter;
    ProgressBar mProgressBar;

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = findViewById(R.id.movies_recycler_view);
        mToolbar = findViewById(R.id.toolbar);
        mSearchView = findViewById(R.id.movies_search_view);
        mProgressBar = findViewById(R.id.movies_loader);

        mProgressBar.bringToFront();

        EditText searchEditText = (EditText) mSearchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        searchEditText.setHintTextColor(getResources().getColor(R.color.white));

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        adapter = new MoviesLayoutAdapter();

        mRecyclerView.setLayoutManager(new GridLayoutManager(this,3));

        mRecyclerView.setAdapter(adapter);

        mMainViewModel = ViewModelProviders.of(MainActivity.this).get(MainViewModel.class);

        mMainViewModel.getAllMoviesLiveData().observe(MainActivity.this, new Observer<List<Datum>>() {

            @Override
            public void onChanged(@Nullable List<Datum> data) {
                mProgressBar.setVisibility(View.GONE);
                if (data != null){
                    adapter.setMoviewData(data);
                }
                else {
                    getDataFromDataBase();
                }
            }
        });

    }

    private void getDataFromDataBase() {
        mMainViewModel.getAllMovies().observe(this, new Observer<List<Datum>>() {
            @Override
            public void onChanged( List<Datum> data) {
                adapter.setMoviewData(data);
            }
        });
    }
}
