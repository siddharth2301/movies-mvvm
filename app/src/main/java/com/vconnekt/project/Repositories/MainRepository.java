package com.vconnekt.project.Repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.util.Log;

import com.vconnekt.project.utils.*;
import com.vconnekt.project.Dao.MoviesDao;
import com.vconnekt.project.Interfaces.RetrofitInterface;
import com.vconnekt.project.Models.Data;
import com.vconnekt.project.Models.Datum;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainRepository {

    private static MainRepository mainRepository;
    RetrofitInterface retrofitInterface;
    MoviesDao moviesDao;
    public LiveData<List<Datum>> liveData;

    public static MainRepository getInstance(Application application){
        if (mainRepository == null){
            mainRepository = new MainRepository(application);
        }
        return mainRepository;
    }

    public MainRepository(Application application){
        retrofitInterface = RertofitClass.getRetrofitService();
        MovieDataBase movieDataBase = MovieDataBase.getInstance(application);
        moviesDao = movieDataBase.moviesDao();
        liveData = moviesDao.getAllMovies();
    }

    public MutableLiveData<List<Datum>> getAllMovies(){
        final MutableLiveData<List<Datum>> mutableLiveData = new MutableLiveData<>();

        retrofitInterface.getMovies().enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                if (response.isSuccessful()){
                    mutableLiveData.setValue(response.body().getData());

                    insertData(response.body().getData());

                }
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {

                mutableLiveData.setValue(liveData.getValue());

            }
        });

        return mutableLiveData;
    }

    private void insertData(List<Datum> data) {
        new InsertData(moviesDao,data).execute();
    }

    class InsertData extends AsyncTask<Void, Void, List<Datum>> {
        MoviesDao moviesDao;
        List<Datum>list;
        InsertData(MoviesDao moviesDao, List<Datum> data){
            this.moviesDao = moviesDao;
            this.list = data;
        }

        @Override
        protected List<Datum> doInBackground(Void... voids) {
            moviesDao.deleteAllMovies();
                for (int i=0;i< list.size();i++){
                   moviesDao.insert(list.get(i));
            }
            return null;
        }
    }


    public LiveData<List<Datum>> getAllData(){
        return liveData;
    }


}
