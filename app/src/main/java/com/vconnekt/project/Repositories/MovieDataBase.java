package com.vconnekt.project.Repositories;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.vconnekt.project.Dao.MoviesDao;
import com.vconnekt.project.Models.Datum;

@Database(entities = {Datum.class}, version = 1)
public abstract class MovieDataBase extends RoomDatabase{

    static MovieDataBase instance;
    private static String DATABASE_NAME = "movie_database";

    public abstract MoviesDao moviesDao();

    public static synchronized MovieDataBase getInstance(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),
                    MovieDataBase.class, DATABASE_NAME)
                    .build();

        }
        return instance;
    }



}
