package com.vconnekt.project.utils;

import com.vconnekt.project.Interfaces.RetrofitInterface;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.vconnekt.project.Interfaces.AppConstants.BASE_URL;

public class RertofitClass {
    private static Retrofit getRetrofitInstance(){
        return new Retrofit.Builder().baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitInterface getRetrofitService(){
        return getRetrofitInstance().create(RetrofitInterface.class);
    }
}
