package com.vconnekt.project.Interfaces;

import com.vconnekt.project.Models.Data;

import retrofit2.Call;
import retrofit2.http.GET;

public interface RetrofitInterface {

    @GET("api/movies")
    Call<Data> getMovies();

}
