package com.vconnekt.project;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.BoundedMatcher;
import android.support.test.espresso.matcher.RootMatchers;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;


import com.vconnekt.project.Activities.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import android.support.test.runner.AndroidJUnitRunner;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.regex.Matcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class FirstTestCode {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityTestRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Test
    public void performSearch() {
        onView(withId(R.id.movies_search_view)).perform(click())               // click() is a ViewAction
                .check(matches(isDisplayed()));

        onView(withId(android.support.v7.appcompat.R.id.search_src_text)).perform(typeText("action"))               // click() is a ViewAction
                .check(matches(isDisplayed()));

    }

    @Test
    public void performClickOnRecyclerView(){
        onView(withId(R.id.movies_recycler_view))
                .inRoot(RootMatchers.withDecorView(
                        Matchers.is(mainActivityTestRule.getActivity().getWindow().getDecorView())))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,click()));
    }

    @Test
    public void performRecyclerViewScroll(){
        RecyclerView recyclerView = mainActivityTestRule.getActivity().findViewById(R.id.movies_recycler_view);

        int item_count = recyclerView.getAdapter().getItemCount();

        onView(withId(R.id.movies_recycler_view))
                .inRoot(RootMatchers.withDecorView(
                        Matchers.is(mainActivityTestRule.getActivity().getWindow().getDecorView())))
                .perform(RecyclerViewActions.scrollToPosition(item_count - 1));
    }

}





